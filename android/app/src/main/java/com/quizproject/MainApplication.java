package com.quizproject;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.idehub.Billing.InAppBillingBridgePackage;
import com.sbugert.rnadmob.RNAdMobPackage;
import com.idehub.GoogleAnalyticsBridge.GoogleAnalyticsBridgePackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNDeviceInfo(),
            new InAppBillingBridgePackage("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmDv/OaKuuc2aegttDvSV8mex2+peSuqjDX4ZzaAhTwCJDa+hR/OdKme2w7VreUyyXHAWiT6ty0eQMs/P8SpoEATZIZdeRFG2/J/As4fTG9RXklb3PFfm4rIQbySGjROZBu42ILZ3OtmBwSKBcEMYfhB9w33tYc5nnnmVlFsWbudDV7snY2HixiLnNOoUW9ALpt3grbozMSPZWtDC2cEM8J3tS37/mfmqYiCAw4xufDQ+Y3WX0Rmrt4PRvaDzzccCA55zJ8ag0tREkCQsz/PKIDwdDX5JUdS8cN2wHGP1zTkPR5GSJXiXmqYUrgFI5zp1J3i59EEFolJGNWBN/JXmMwIDAQAB"),
            new RNAdMobPackage(),
            new GoogleAnalyticsBridgePackage()
      );
    }
    
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

}
