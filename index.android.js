import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux'
import App from './src/containers/App'
import configureStore from './src/store/configureStore'
import { View,Text } from 'react-native'
import Loading from './src/components/Loading'
//const store = configureStore();

export default class QuizProject extends Component {

  constructor(props){
    super(props);
    this.state = {
        isLoading: true,
        store: configureStore(() => {
          this.setState({ isLoading: false });          
        }),
      };
  }

  render() {
    if(this.state.isLoading){
      return <Loading showMsg={false} />
    }

    return (
      <Provider store={this.state.store}>
        <App />
      </Provider>
    )
  }
}

AppRegistry.registerComponent('QuizProject', () => QuizProject);
