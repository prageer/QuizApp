export const SET_QUESTIONS = 'SET_QUESTIONS';
export const SET_DEVICEID = 'SET_DEVICEID';
export const SET_RECORDS = 'SET_RECORDS';
export const CHECK_WRONGANSWER = 'CHECK_WRONGANSWER';
export const INCREASE_ACTIVEINDEX = 'INCREASE_ACTIVEINDEX';
export const DECREASE_ACTIVEINDEX = 'DECREASE_ACTIVEINDEX';
export const GO_ONLINE = 'GO_ONLINE';
export const GO_OFFLINE = 'GO_OFFLINE';
export const MODIFY_OFFLINE = 'MODIFY_OFFLINE';
export const ADD_RECORDS_OFFLINE = 'ADD_RECORDS_OFFLINE';
export const MAKE_PAY = 'MAKE_PAY';
export const NO_ADS = 'NO_ADS';


export function initLoad(items) {

	return (dispatch) => {
        // inform Get Status API is starting
        dispatch(setQuestions(items));
    };
}

export function initRecords(items){
    return (dispatch) => {
        // inform Get Status API is starting
        dispatch(setRecords(items));
    };
}

export function setQuestions(questions) {
    return {
        type: SET_QUESTIONS,
        questions:questions
    };
}

export function checkWrongAnswer(){
    return {
        type: CHECK_WRONGANSWER
    };
}

export function setRecords(records) {
    return {
        type: SET_RECORDS,
        records:records
    };
}

export function setDeviceId(deviceId) {
    return {
        type: SET_DEVICEID,
        deviceId
    };
}

export function incActiveIndex(is_correct, level) {
    return {
        type: INCREASE_ACTIVEINDEX,
        is_correct,
        level
    };
}
export function decActiveIndex() {
    return {
        type: DECREASE_ACTIVEINDEX
    };   
}

export function goOnline(){
    return {
        type: GO_ONLINE
    };
}

export function goOffline(){
    return {
        type: GO_OFFLINE
    };
}

export function addRecordsOffline(item, is_correct, level){
    return {
        type: ADD_RECORDS_OFFLINE,
        item,
        is_correct,
        level
    };
}

export function modifyOffline(modify){    
    return {
        type: MODIFY_OFFLINE,
        modify
    };
}

export function makePay(){
    return {
        type: MAKE_PAY
    };
}

export function makeNoAds(noAds){
    return {
        type: NO_ADS,
        noAds
    };
}
