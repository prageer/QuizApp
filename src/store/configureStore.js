import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from '../reducers'
import { AsyncStorage } from 'react-native';
import { persistStore, autoRehydrate } from 'redux-persist'; // add new import

import { createMiddleware } from 'redux-beacon';
import { GoogleAnalytics } from 'redux-beacon/targets/react-native';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';

import config from '../../config';

const target = GoogleAnalytics(config.TRACKING_ID, GoogleAnalyticsTracker);

const eventsMap = {
  'SET_QUESTIONS': {
    eventFields: (action, prevState) => ({
      hitType: 'event',
      eventCategory: 'Counter',
      eventAction: 'setQuestions',
    })
  },
  'SET_RECORDS': {
    eventFields: (action, prevState) => ({
      hitType: 'event',
      eventCategory: 'Counter',
      eventAction: 'setRecords',
    })
  },
};

const analyticsMiddleware = createMiddleware(eventsMap, target);


const middleWare = [thunk, analyticsMiddleware];

const createStoreWithMiddleware = applyMiddleware(...middleWare)(createStore);

export default configureStore = (onComplete) => {
  const store = autoRehydrate()(createStoreWithMiddleware)(reducer);
  persistStore(store, { storage: AsyncStorage }, onComplete);
  
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}; 
  
