import { Platform, NativeModules} from 'react-native';
import InAppBilling from 'react-native-billing';
import config from '../../config';
import ReactNative from 'react-native';

const { Alert } = ReactNative;

const { InAppUtils } = NativeModules

var isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

export function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

export function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {      
        var result = (parseInt(a[property]) < parseInt(b[property]) ) ? -1 : (parseInt(a[property]) > parseInt(b[property])) ? 1 : 0;
        return result * sortOrder;
    }
}

export function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

export function compare2Arr(a, b){
  let ini_a = a.map(function(item, index){
    return item["_key"];
  });
  
  let c = [].concat(b.filter(function (item) {
    return ini_a.indexOf(item["_key"]) < 0;
  }));  

  let ini_b = b.map(function(item, index){
    return item["_key"];
  }); 

  let d = ini_a.map(function(item, index){
    let t = ini_b.indexOf(item)
    if( t >= 0){
      return b[t];
    }
  });
  
  let r = d.concat(c);
  let v = r.filter(function(item){
    return (item);
  });

  return v;
}

export function checkAnswers(a, b)
{

  let ini_a = a.map(function(item, index){
    return item["_key"];
  });

  let ini_b = b.map(function(item, index){
    return item["_key"];
  }); 

  let d = ini_a.map(function(item, index){
    if( ini_b.indexOf(item) < 0){
      return a[index];
    }
  });


  let e = ini_b.map(function(item, index){
    for(var i=0; i<a.length; i++){
      if( a[i]["_key"] == item ){
        if( a[i]["correct_text"] != b[index]["user_answer_text"] )
          return a[i];
      }
    }
  });

  let f = ini_b.map(function(item, index){
    for(var i=0; i<a.length; i++){
      if( a[i]["_key"] == item ){
        if( a[i]["correct_text"] == b[index]["user_answer_text"] )
          return a[i];
      }
    }
  });

  f = f.filter(function(item){
    return (item);
  }); 

  /*
  if(e.length >0 ){
    d=shuffle(d);
  }
  */

  let k = [].concat(f,e,d);

  let v = k.filter(function(item){
    return (item);
  }); 

  let activeIndex = f.length;

  if( activeIndex == a.length )
    activeIndex = a.length -1;
  if( activeIndex < 0 )
    activeIndex = 0;
  return [v, activeIndex];
}

export function addAnswer(a, b){
  let duplicate = false;
  let c = a.map( (item, index)=>{
    if( item["_key"] == b[0]["_key"] ){
      duplicate = true;
      return b[0];
    }
    else
      return item;
  });

  let d = [];
  if(!duplicate){
    d = c.concat(b[0]);
  }else{
    d = c;
  }

  return d;
}

//questions, answers, activeIndex, is_correct
export function findNext(a, b, is_correct, ac, level)
{


  let c = a.slice(0, ac+1);
  let d = a.slice(ac+1, a.length);
  

  if( is_correct ){

    let e = d.map((item, index)=>{
      if(item.level>=level)
        return item.level;
    })

    e=e.filter((item,index)=>{ return item!=undefined });
    let min_level = Math.min(...e);

    if(!isNumeric(min_level)){
      //return c+d
      //return ac+1
      let k = [].concat(c,d);
      let v = k.filter(function(item){
        return (item);
        }); 
      return [v, (ac+1)];
    }

    let s = d.filter((item, index)=>{
      if(item.level==min_level)
        return true;
    });

    let next = s[0];
    let follow = d.filter((item, index)=>{
      if( item.question_text != next.question_text )
        return true;
    });

    //return c+next+follow
    //return ac+1
    let k = [].concat(c, next, follow);
    let v = k.filter(function(item){
      return (item);
      }); 
    return [v, (ac+1)];
  }else{
    
    let e = d.map((item, index)=>{
      if(item.level<=level)
        return item.level;
    })

    e=e.filter((item,index)=>{ return item!=undefined });
    let max_level = Math.max(...e);
    if(!isNumeric(max_level)){

      //return c+d
      //return ac+1
      let k = [].concat(c, d);
      let v = k.filter(function(item){
        return (item);
        }); 
      return [v, (ac+1)];
    }

    let s = d.filter((item, index)=>{
      if(item.level==max_level)
        return true;
    });

    let next = s[0];
    let follow = d.filter((item, index)=>{
      if( item.question_text != next.question_text )
        return true;
    });

    //return c+next+follow
    //return ac+1
    let k = [].concat(c, next, follow);
    let v = k.filter(function(item){
      return (item);
      }); 
  
    return [v, (ac+1)];
  }
}

export function payMonth(props){
  if (Platform.OS === 'ios')
  {
    //props.makePay();
 
    var products = [
       config.IOS_PRODUCT_ID
    ];
    InAppUtils.loadProducts(products, (error, products) => {
       
       var productIdentifier = config.IOS_PRODUCT_ID;
        InAppUtils.purchaseProduct(productIdentifier, (error, response) => {
           // NOTE for v3.0: User can cancel the payment which will be availble as error object here.
           if(response && response.productIdentifier) {
              //AlertIOS.alert('Purchase Successful', 'Your Transaction ID is ' + response.transactionIdentifier);
              //unlock store here.
              props.makePay();
           }else{
             
              Alert.alert(
                config.PURCHASE_ERROR_TITLE,
                config.PURCHASE_ERROR_CNT,
                [       
                  {text: 'OK', onPress: () => {  } },
                ]
              )
				
           }
        });

    });     
  }
  else
  {
    //props.makePay();   
    
    InAppBilling.open().
    then(() => InAppBilling.purchase(config.ANDROID_PRODUCT_ID))
    .then((details) => {      
      return InAppBilling.getProductDetails(config.ANDROID_PRODUCT_ID);
    })
    .then((productDetails) => {
      
      props.makePay();

      return InAppBilling.close();
    })
    .catch((error) => {

      Alert.alert(
        config.PURCHASE_ERROR_TITLE,
        config.PURCHASE_ERROR_CNT,
        [       
          {text: 'OK', onPress: () => {  } },
        ]
      )
      
    });
     
  }
}