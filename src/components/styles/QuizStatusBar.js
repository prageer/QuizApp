import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({  
  statusbar: {
    backgroundColor:'#0abda0',
    paddingTop:5,
    paddingBottom:5,
    flexDirection:'row'    
  },
  toolbarButton:{
    width: 50,            
    color:'#fff',
    textAlign:'center'
  },
  toolbarTitle:{
    color:'#fff',
    textAlign:'center',
    fontWeight:'bold',
    flex:1                
  }
});

export default styles;