import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({ 
 
  li: {
    backgroundColor: '#fff',
    borderBottomColor: '#eee',
    borderColor: 'transparent',
    borderWidth: 2,
    paddingLeft: 16,
    paddingTop: 14,
    paddingBottom: 16,
    color:'black',
    fontSize: 19
  },
  backRed: {
    backgroundColor: '#ed1c24',
    color:'white'
  },
  backBlue: {
    backgroundColor: '#0abda0',
    color:'white'
  }
});

export default styles;