import React, { Component } from 'react';
import {
  StyleSheet  
} from 'react-native';

const styles = StyleSheet.create({ 
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  questionImage:{
    /*
    width:'100%', 
    height:'40%'
    */    
  },
  questionText:{    
    paddingLeft:10, 
    paddingRight:10, 
    fontSize:18, 
    fontWeight:'bold', 
    color:'black', 
    textAlign:'center', 
    lineHeight:30,
    paddingTop:10,
    paddingBottom:10
  },
  admobButton:{
    color: '#333333',
    marginBottom: 15
  }
});

export default styles;