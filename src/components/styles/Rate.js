import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({ 
 
  ratePanel: {
  	//backgroundColor:'#1ecbc5'
  	backgroundColor:'#0abda0',
  	borderLeftWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderWidth:1,
    borderColor:'#0abaaa'
  },
  desc:{
  	color:'white',
  	textAlign:'center',
  	fontSize:17,
  	marginTop:10,
  	marginBottom:10
  },
  buttonPanel:{
  	flexDirection: 'row', 
  	justifyContent: 'space-around',
  	marginBottom:15
  },
  btn:{
  	borderWidth: 1
  },
  buttonText:{
	fontSize:15,
	textAlign:'center',
	margin:10,
	color:'white'
  },
  buttonTextPress:{
	fontSize:15,
	textAlign:'center',
	margin:10,
	color:'#0abda0'
  },
  button:{
	borderColor:'white',
	borderWidth:1,
	borderRadius:10
  },
  buttonPress:{
	borderColor:'#0abda0',
	backgroundColor:'white',
	borderWidth:1,
	borderRadius:10
  }
});

export default styles;