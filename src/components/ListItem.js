import React, {Component} from 'react';
import ReactNative from 'react-native';
import styles from './styles/ListItem';

const { View, TouchableHighlight, Text, Alert } = ReactNative;
import {payMonth} from '../service/util';
import config from '../../config';

class ListItem extends Component {

	constructor(props){
		super(props);
		this.state = {
	      isCorrect: false,
	      isPressed: false
	    };
	}

	onPress(){
		if(this.props.selected) return;

		//if( this.props.activeIndex >= )
		
		let pass = this.props.checkActive(this.props.activeIndex);
		let alertMessage = config.PURCHASE_REQUIRE_CNT;
		let alertTitle = config.PURCHASE_REQUIRE_TITLE;
		if(!pass){
			Alert.alert(
				alertTitle,
				alertMessage,
				[
					{text: 'Cancel', onPress: () => {} },
					{text: 'OK', onPress: () => { payMonth(this.props.actions); } },
				]
			)

			return;
		}

		let correct = this.props.item === this.props.correct_text;
		this.setState({ 
			isCorrect:correct,
			isPressed:true
		} );

		this.props.onSelect(this.props.item, correct, this.props.level);

		setTimeout(()=>{
			this.setState({ 				
				isPressed:false
			} );
		},300);
	}
  render() {

  	const {correct_text, prefix} = this.props;
  	const {isCorrect, isPressed} = this.state;

  	var myStyle = styles.li;
  	if( isCorrect && isPressed )
  		myStyle = [styles.li, styles.backBlue];
  	else if( !isCorrect && isPressed )
  		myStyle = [styles.li, styles.backRed];  	

  	if( !this.props.selected ){
  		myStyle = styles.li;  		
  	}
  	
    return (
      <TouchableHighlight onPress={this.onPress.bind(this)}>
        <Text style={myStyle} >{prefix}.  {this.props.item}</Text>
      </TouchableHighlight>
    );
  }
}

export default ListItem;