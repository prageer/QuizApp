import React, {Component} from 'react';
import ReactNative from 'react-native';
import styles from './styles/QuizStatusBar';

const { StyleSheet, Text, View} = ReactNative;

class QuizStatusBar extends Component {
  constructor(props){
    super(props);
  }

  render() {

    const {payed, noAds} = this.props;    

    const {onPressNoAds, onShowRatePanel} = this.props;

    let statusBar = null;
    let noAdsText = null;

    if( noAds || payed )
      noAdsText = (<Text style={styles.toolbarButton}></Text>)    
    else
      noAdsText = (<Text style={styles.toolbarButton} onPress={onPressNoAds}>No Ads</Text>)    

    return (
      <View>
        <View style={styles.statusbar}>
          <Text style={styles.toolbarButton} onPress={onShowRatePanel}>Rate</Text>
          <Text style={styles.toolbarTitle}>{this.props.title}</Text>
          {noAdsText}
        </View>        
      </View>
    );
  }
}

export default QuizStatusBar;