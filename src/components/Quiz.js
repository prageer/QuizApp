import React, {Component} from 'react';
import {StyleSheet, ScrollView, View, Text, TouchableOpacity,ListView, Image, StatusBar, TouchableHighlight, Alert} from 'react-native';
import QuizStatusBar from './QuizStatusBar';
import ListItem from './ListItem';
import Loading from './Loading';
import Rate from './Rate';
import styles from './styles/Quiz';
import {getQuestions, setRecords, checkOnLine} from '../firebase';
import DeviceInfo from 'react-native-device-info';
import {shuffle, payMonth} from '../service/util';
import FitImage from 'react-native-fit-image';
import config from '../../config'

import { 
  AdMobBanner, 
  AdMobInterstitial, 
  PublisherBanner,
  AdMobRewarded
} from 'react-native-admob'

// initialize the admob
AdMobInterstitial.setAdUnitID(config.ADMOBINTERSTITIAL_ID);

export default class Quiz extends Component {
  constructor(props) {
    super(props);

    // check online state
    checkOnLine(props);

    this.state={
      initLoading:true,
      childItemSelected:false,
      showRate:false,
      showBannerWithRate:true,
      showOfflineMsg:false
    }    

    // init answers order
    this.firebaseFiled = ['correct_text', 'incorrect_text1', 'incorrect_text2', 'incorrect_text3', 'incorrect_text4', 'incorrect_text5'];    
    this.order_arr = shuffle( this.firebaseFiled );   

    // record the number of round from the begining 
    this.init = 0;    
  }

  async getOnlineState() {
    setTimeout(()=>{
      let item = this.props.questions[this.props.activeIndex];      
      if( !item && !this.props.online){
        this.setState({showOfflineMsg:true});
        Alert.alert(
          config.OFFLINE_ALERT_TITLE,
          config.OFFLINE_ALERT_CNT,
          [
            {text: 'OK', onPress: () => { } },
          ]
        )
      }
    }, 6000);    
  }


  componentWillMount(){
    // set Device Id
    this.props.setDeviceId( DeviceInfo.getUniqueID() );

    // get questions & records from Firebase
    getQuestions(this.props, ()=>{ this.setState({initLoading:false}) });        

    // check the previous wrong answers    
    this.props.checkWrongAnswer();
    this.props.makeNoAds(false);
    
  }

  onSelect(user_answer_text, is_correct, level){
    //alert(this.props.online);
    this.setState({
      childItemSelected:true
    })

    if( this.init % 3==2 && this.checkShowBanner()){
      AdMobInterstitial.requestAd(error => {
        AdMobInterstitial.showAd((error) => {});
      });
    }

    if( this.init>1 && this.init % 10==0 ){
      this.onShowRatePanel();
    }else{
      this.onPressAds();
    }
      
    
    //initialize
    setTimeout(()=>{     
      // change answers order 
      this.order_arr = shuffle( this.firebaseFiled );    

      this.setState({
        childItemSelected:false
      })
      this.init++;

      // send reocrds to firebase 
      setRecords( this.props.questions[this.props.activeIndex], this.props.deviceId, user_answer_text, is_correct, this.props, level );      

    }, 500);
    
  }

  checkPayDate(){
    let current = Math.round(new Date().getTime()/1000);
    let d = new Date();
    let m = d.getMonth();
    d.setMonth(d.getMonth() - 1);
    if (d.getMonth() == m) d.setDate(0);
    d.setHours(0, 0, 0);
    d.setMilliseconds(0);
    let agoMonthTimestamp = (d/1000|0);
    
    if( this.props.payDate && this.props.payDate > agoMonthTimestamp){
      return true;
    }else
      return false;
  }

  checkActive(activeIndex){    
    if( this.checkPayDate() ){      
      return true;
    }
    else{
      if( activeIndex >= config.ALLOWABLE_ACCOUNT )
        return false;
      else
        return true;
    }
  }
  
  componentDidMount() {
    this.getOnlineState().done();

    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > 500){
        break;
      }
    }    
  }

  componentDidUpdate(prevProps, prevState){    

    if( !this.state.childItemSelected ){   
      if( this.init >=1 ){
        if(prevProps.activeIndex != this.props.activeIndex){
          console.log(prevProps.activeIndex+"---"+this.props.activeIndex);
          this.refs._scrollView.scrollTo({x:0, y:0, animated:true});              
        }
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.questions.length >0 || nextState.showOfflineMsg) {
      return true;
    }else{
      return false;
    }
  }

  renderItem(item) {    

    let prefixArr = ['A','B','C','D','E','F'];

    let count = 0;
    return this.order_arr.map((key, index)=>{      
      if( item[key] !="" ){
        count++;
        return  <ListItem 
                  item={item[key]} 
                  key={index} 
                  prefix={prefixArr[count-1]} 
                  correct_text={item.correct_text} 
                  selected={this.state.childItemSelected} 
                  level={item.level} 
                  onSelect={this.onSelect.bind(this)} 
                  checkActive = {this.checkActive.bind(this)}
                  activeIndex={this.props.activeIndex} 
                  actions={this.props}
                /> 
      }
    })  

  }

  onPressNoAds(){
    //if()

    let pass = this.checkPayDate();
    let alertMessage = config.PURCHASE_REQUIRE_CNT;
    let alertTitle = config.PURCHASE_REQUIRE_TITLE;
    if(!pass){
      Alert.alert(
        alertTitle,
        alertMessage,
        [
          {text: 'Cancel', onPress: () => {} },
          {text: 'OK', onPress: () => { payMonth(this.props); } },
        ]
      )

      return;
    }else{
      this.props.makeNoAds(true);
    }
  }

  /*
    show Ads & no Rate Panel
  */
  onPressAds(){
    this.setState({showRate:false});
    this.setState({showBannerWithRate:true});
  }

  /*
    no Ads & show Rate Panel
  */
  onShowRatePanel(){    
    this.setState({showBannerWithRate:false});
    this.setState({showRate:true});
  } 

  checkShowBanner(){
    
    let showBanner = false;
    let payed = this.checkPayDate();
    let noAds = this.props.noAds;
     
    if(!payed){
      if(noAds)
        showBanner = false;
      else
        showBanner = true;
    }
    else{
      showBanner = false;
    }

    return showBanner;
  }

  checkShowRatePanel(){

    return this.state.showRate;
  }

  render() {
    const {questions, activeIndex, noAds} = this.props
    const {showBannerWithRate} = this.state;
    const item = questions[activeIndex];
    const payed = this.checkPayDate();

    if( this.state.showOfflineMsg ){
      return <Loading showMsg = {true} />      
    }

    if(this.state.initLoading)
       return <Loading showMsg = {false} />

    if( !item )
      return <Loading showMsg = {false} />

    return (
        <View style={styles.container}>
          <StatusBar hidden />
          <QuizStatusBar 
            title="IQ Test" 
            payed={payed} 
            noAds={noAds} 
            onPressNoAds = {this.onPressNoAds.bind(this)} 
            onShowRatePanel={this.onShowRatePanel.bind(this)} 
          />

          <ScrollView style={{ marginBottom:0}} ref='_scrollView'>
            {(item.question_image_url !=="") && 
              <FitImage 
              style={styles.questionImage} 
              resizeMode="stretch" 
              source={{uri: `${item.question_image_url}`}} />}

            <Text style={styles.questionText}>
              {item.question_text}
            </Text>
          
            {this.renderItem(item)}
          </ScrollView>

          {this.checkShowRatePanel() && 
            <Rate
              onPressAds={this.onPressAds.bind(this)}             
            />}
          
          {(this.checkShowBanner() && showBannerWithRate )&& 
            <AdMobBanner            
            bannerSize="BANNER"
            adUnitID={config.ADMOBBANNER_ID}            
            didFailToReceiveAdWithError={(error)=>console.log(error)} /> }

        </View>        
    );
  }
}
