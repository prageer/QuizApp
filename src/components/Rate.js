import React, {Component} from 'react';
import ReactNative from 'react-native';
import {Platform} from 'react-native';
import config from '../../config';
import styles from './styles/Rate';

const { StyleSheet, Text, View, Button, Alert, Linking, TouchableHighlight } = ReactNative;

class Rate extends Component {
  constructor(props){
    super(props);    
    this.state={pressStatus:false}
  }

  onHideunderlayy=()=>{
    this.setState({pressStatus:false, pressYesStatus:false});
  }
  
  onShowUnderlayy=()=>{
    this.setState({pressStatus:true, pressYesStatus:true});
  }

  onRatePress(){
        
    if(Platform.OS =='ios'){
        
        Linking.canOpenURL(config.APP_STORE_LINK).then(supported => {
          if (!supported) {
            console.log('Can\'t handle url: ' + url);
          } else {
            return Linking.openURL(config.APP_STORE_LINK);
          }
        }).catch(err => console.error('An error occurred', err));

        //Linking.openURL(config.APP_STORE_LINK).catch(err => console.error('An error occurred', err));
    }
    else{
        Linking.canOpenURL(config.PLAY_STORE_LINK).then(supported => {
          if (!supported) {
            console.log('Can\'t handle url: ' + url);
          } else {
            return Linking.openURL(config.PLAY_STORE_LINK);
          }
        }).catch(err => console.error('An error occurred', err));

        //Linking.openURL(config.PLAY_STORE_LINK).catch(err => console.error('An error occurred', err));
    }

    this.props.onPressAds();
    
  }

  render() {

    const {onPressAds} = this.props;

    return (
        <View style={styles.ratePanel}>
          <Text style={styles.desc}>Would you mind rating our app?</Text>
          <View style={styles.buttonPanel}>

            <TouchableHighlight
             activeOpacity={1}
             style={this.state.pressStatus?styles.buttonPress:styles.button}
             onHideUnderlay={this.onHideUnderlay}
             onShowUnderlay={this.onShowUnderlay}
	     onPress={onPressAds} 
            >
              <Text style={this.state.pressStatus?styles.buttonTextPress:styles.buttonText}>No, Thanks</Text>
            </TouchableHighlight>

	    <TouchableHighlight
             activeOpacity={1}
             style={this.state.pressYesStatus?styles.buttonPress:styles.button}
             onHideUnderlay={this.onHideUnderlay}
             onShowUnderlay={this.onShowUnderlay}
	     onPress={this.onRatePress.bind(this)} 
            >
              <Text style={this.state.pressYesStatus? styles.buttonTextPress: styles.buttonText}>Sure, I’ll rate</Text>
            </TouchableHighlight>
          </View>   
        </View>
      
    );
  }
}

export default Rate;