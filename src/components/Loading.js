import React, {Component} from 'react';
import ReactNative from 'react-native';

const { StyleSheet, Text, View} = ReactNative;

class Loading extends Component {
  constructor(props){
    super(props);
  }
  render() {

    let loadingText = null;

    if(!this.props.showMsg){
      loadingText = (
            <View>
              <Text style={{color:'white' }}>Loading...</Text>
            </View>
          );
    }

    return (
      
        <View style={{ flex:1, flexDirection:'column', backgroundColor:'#0abda0', justifyContent:'center', alignItems:'center'}}>
          <View style={{marginBottom:40, marginTop:-100}}>
            <Text style={{color:'white', fontSize:30, fontWeight:'bold'}}>Quiz App</Text>
          </View>

          {loadingText}

        </View>        
      
    );
  }
}

export default Loading;