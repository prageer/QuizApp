import {
  SET_QUESTIONS,
  SET_DEVICEID,
  INCREASE_ACTIVEINDEX,
  SET_RECORDS,
  DECREASE_ACTIVEINDEX,
  CHECK_WRONGANSWER,
  GO_ONLINE,
  GO_OFFLINE,
  MODIFY_OFFLINE,
  ADD_RECORDS_OFFLINE,
  MAKE_PAY,
  NO_ADS
} from '../actions/items'

import {shuffle, randomIntFromInterval, dynamicSort, compare2Arr, checkAnswers, addAnswer, findNext } from '../service/util';

const initialState = {
  questions: [],
  records: [],  
  rehydratLoading: true,
  applyingAnswer: true,
  
  deviceId: '',
  activeIndex:0,
  
  payDate:0,

  noAds:false,

  modifyWhenOffline:false,
  online:false
}

export default function reducer(state = initialState, action) {
  
  switch (action.type) {  
    case SET_QUESTIONS:  

      let ori_questions = state.questions;
      let new_questions = [];     

      let temp_questions = shuffle(action.questions);            
      //temp_questions.sort(dynamicSort("level"));      


      let length = temp_questions.length;
      let randomInt = randomIntFromInterval(0, length-1);

      if( ori_questions.length == 0 ){  
        new_questions = [temp_questions[randomInt]].concat(temp_questions.slice(randomInt + 1), temp_questions.slice(0, randomInt) );
      }else{        
        new_questions = compare2Arr( ori_questions, temp_questions);
      }

      return {
        ...state,
        questions: new_questions,
        length: length
      }
      /*
      return {
        ...state,
        questions: [],
        length: 0
      }
      */

    case CHECK_WRONGANSWER:

      ori_questions = state.questions;

      let ori_answers = state.records;
     
      let res_arr = checkAnswers(ori_questions, ori_answers);      
      
      return {
        ...state,
        questions: res_arr[0],
        activeIndex: res_arr[1]
      }
      
    case SET_RECORDS:
      return {
        ...state,
        records: action.records
      }

    case SET_DEVICEID:  
      return {
        ...state,
        deviceId: action.deviceId
      }
    case GO_ONLINE:  
      return {
        ...state,
        online: true
      }
    case GO_OFFLINE:  
      return {
        ...state,
        online: false,
        fetchNet: true
      }

    case NO_ADS:  
      return {
        ...state,
        noAds: action.noAds
      }

    case ADD_RECORDS_OFFLINE:
      let ori_records = state.records;
      let list = addAnswer( ori_records, [action.item] );

          
      let ac = state.activeIndex;      
      
      ori_questions = state.questions;
      ori_answers = list;
      if((ac+1) >= ori_questions.length ){       

        //console.log(ori_questions);              
        res_arr = checkAnswers(ori_questions, ori_answers);      
        
        return {
          ...state,
          questions: res_arr[0],
          activeIndex: res_arr[1],
          records:list
        }
      }else{

        res_arr = findNext(ori_questions, ori_answers, action.is_correct, ac, action.level);
        return {
          ...state,
          questions: res_arr[0],
          activeIndex: res_arr[1],
          records:list
        }
      }      

    case DECREASE_ACTIVEINDEX:            
      ac = state.activeIndex;
      ac = ac - 1;
      if(ac < 0 )
        ac = 0;

      return {
        ...state,
        activeIndex: ac
      }
    case MODIFY_OFFLINE:

      return {
        ...state,
        modifyWhenOffline: action.modify
      }
    case MAKE_PAY:
      return {
        ...state,
        payDate: Math.round(new Date().getTime()/1000)        
      }

    default:
      return state
    }
}
