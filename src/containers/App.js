import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Quiz from '../components/Quiz'
import * as ItemsActions from '../actions/items'

function mapStateToProps(state) {

  return {
  	questions: state.items.questions,
	records: state.items.records,
	applyingAnswer: state.items.applyingAnswer,
	deviceId: state.items.deviceId,
	activeIndex: state.items.activeIndex,
	online:state.items.online,
	modifyWhenOffline:state.items.modifyWhenOffline,
	payed:state.items.payed,
	payDate:state.items.payDate,
	noAds:state.items.noAds,
	online:state.items.online
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ItemsActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Quiz)