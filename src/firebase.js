import { initializeApp } from 'firebase'
import { initLoad } from './actions/items'
import config from '../config'
import DeviceInfo from 'react-native-device-info';
import NetInfo from 'react-native';

const firebaseApp = initializeApp({
  apiKey: config.API_KEY,
  authDomain: config.AUTH_DOMAIN,
  databaseURL: config.DATABASE_URL,
  storageBucket: config.STORAGE_BUCKET
})



export const questionsRef = firebaseApp.database().ref('questions');
export const recordsRef = firebaseApp.database().ref('records/'+DeviceInfo.getUniqueID()+'/');

const connectedRef = firebaseApp.database().ref('.info/connected');

function getQuestion(actions, cb){
  questionsRef.on('value', (snap) => {
      var items = [];
      snap.forEach((child) => {
        items.push({
          question_image_url: child.val().question_image_url,
          question_text: child.val().question_text,
          correct_text: child.val().correct_text,
          incorrect_text1: child.val().incorrect_text1,
          incorrect_text2: child.val().incorrect_text2,
          incorrect_text3: child.val().incorrect_text3,
          incorrect_text4: child.val().incorrect_text4,
          incorrect_text5: child.val().incorrect_text5,
          level: child.val().level,
          _key: child.key
        });
      });

      actions.initLoad(items);      
  });

  
    let count = 0;
    actions.records.filter((item, index)=>{
      const subRecordsRef = firebaseApp.database().ref('records/'+DeviceInfo.getUniqueID()+'/'+item["_key"]+'/');    
      let record_item = {    
        date_time: item.date_time,
        question_image_url: item.question_image_url,
        question_text:item.question_text,
        user_answer_text:item.user_answer_text,
        is_correct:item.is_correct
      };

      subRecordsRef.set( record_item, ()=>{ 
        count++; 
        if(count == actions.records.length){

            recordsRef.on('value', (snap) => {
              var items = [];
              snap.forEach((child) => {
                items.push({
                  date_time: child.val().date_time,
                  question_image_url: child.val().question_image_url,
                  question_text: child.val().question_text,
                  user_answer_text: child.val().user_answer_text,          
                  is_correct: child.val().is_correct,          
                  _key: child.key
                });
              });

              actions.initRecords(items);
            });
          
        }
      } );
       
      return true;
    })    

    cb();
}

export function getQuestions(actions, cb){  


  firebaseApp.auth().onAuthStateChanged(user => {
    if (user) {
      //getQuestion(actions,cb);
    } else {
        firebaseApp.auth().signInAnonymously().then(user => {
          // store token
          user.getToken().then(token => { 
            //getQuestion(actions,cb) 
          })
        }, (err) => {
          // log error
        });
    }
  }, (err) => {
    // log error
  });


  getQuestion(actions,cb);
  
}

export function setRecords(item, deviceId, user_answer_text, is_correct, props, level){  

  let record_item = {    
    date_time: Math.floor(Date.now() / 1000),
    question_image_url: item.question_image_url,
    question_text:item.question_text,
    user_answer_text:user_answer_text,
    is_correct:is_correct,
    _key:item["_key"]
  };

    props.addRecordsOffline(record_item, is_correct, level);
    const subRecordsRef = firebaseApp.database().ref('records/'+deviceId+'/'+item["_key"]+'/');
    subRecordsRef.set( record_item )  
}

export function checkOnLine(actions) {

  connectedRef.on('value', snap => {
    if (snap.val() === true) {
      actions.goOnline();
    } else {
      actions.goOffline();
    }
  })

}
